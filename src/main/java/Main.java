import classes.Global;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import utils.Util;

public class Main {
    public static void main(String... args){
        // attach vlcj and create single frame instance
        new NativeDiscovery().discover();
        Global.getInstance()
                .constructBorder(Util.getJsonConfigJson().getInt("cameraAmount")); // количество камер решил в json вынести и цеплять его через utils.Util
        // подключаем vlcj
        // boolean found = new NativeDiscovery().discover();
        // System.out.println(found);
        // System.out.println(LibVlc.INSTANCE.libvlc_get_version());
    }
}
