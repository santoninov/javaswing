package interfaces;

import classes.LeftPanelViewCell;

import javax.swing.*;

@FunctionalInterface
public interface LeftPanelViewCellBuilderInterface {
    LeftPanelViewCell build(final JFrame frame, final JPanel parent);
}
