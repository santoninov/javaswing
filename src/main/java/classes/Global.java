package classes;

import interfaces.LeftPanelViewCellBuilderInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

//ctrl + shift + {1..9} - поставить закладку на строку (даже на содержимое)
//ctrl + alt + m на выделенный текст - пихает его в функцию

public class Global {

    private static Global global = null;

    // TODO autoscreensize
    private final int SCREEN_HEIGHT = 768;
    private final int SCREEN_WIDTH = 1024;
    private final JFrame frame;
    private final JPanel parent;

    /** main frame initialization and listener to release the resources after window closing */
    private Global() {
        parent = new JPanel();
        frame = new JFrame("Main Window");
        frame.setLayout(new GridLayout(0,2));
        frame.setBounds(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        frame.setBackground(Color.WHITE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * singleton instance
     * @return instance
     */
    public static synchronized Global getInstance(){
        if(global == null){
            return new Global();
        }
        return global;
    }

    /**
     * create panel, start threads for every panel cell, add then them to main frame
     * @param camerasCount - cameras amount
     */
    public void constructBorder(int camerasCount){
        parent.setLayout(new GridLayout(camerasCount,0));
        LeftPanelViewCellBuilderInterface cellBuilder = LeftPanelViewCell::new;
        for (int i = 0; i < camerasCount; i++)
            SwingUtilities.invokeLater(cellBuilder.build(frame,parent));
        frame.add(parent);
        frame.add(new JPanel());
        frame.setVisible(true);
        // ну вообще, вообще, фрэйм ждет пока закончатся потоки чтоб добавить панель в себя.
        // то есть работает с уже сдлеланными объектами. Я не знаю. там вродь есть инвок энд вэйт. Посмореть ваще как они там работают
        // эта поточность, если честно, меня смущает здесь
//        for (int i = 0; i < camerasCount; i++)
//            SwingUtilities.invokeLater(() -> workWithCurrentThread(parent));
    }

    /**
     * rotation function(ctrl + c | ctrl + v https://stackoverflow.com/questions/37758061/rotate-a-buffered-image-in-java/37758533)
     * ну так, оставлю на будущее. Мож пригодится
     * @param img
     * @param angle
     * @return
     */
    public BufferedImage rotateImageByDegrees(BufferedImage img, double angle) {

        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = img.getWidth();
        int h = img.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotated.createGraphics();
        AffineTransform at = new AffineTransform();
        at.translate((newWidth - w) / 2, (newHeight - h) / 2);

        int x = w / 2;
        int y = h / 2;

        at.rotate(rads, x, y);
        g2d.setTransform(at);
        g2d.drawImage(img, 0, 0, null);
        g2d.setColor(Color.RED);
        g2d.drawRect(0, 0, newWidth - 1, newHeight - 1);
        g2d.dispose();

        return rotated;
    }

}
