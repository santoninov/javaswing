package classes;

import org.json.JSONArray;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import utils.Util;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class LeftPanelViewCell implements Runnable {

    private final JFrame frame;
    private final JPanel parent;
    private List<Item> boxItemsList;
    private int currentCamera;

    // вообще передавать и хранить в каждом истансе потока ссылки на панель и фрейм в глобале
    // не самое лучшее решение, думаю. В конце концов есть di и все дела. Но пусть пока так побудет.
    public LeftPanelViewCell(final JFrame frame, final JPanel parent) {
        this.frame = frame;
        this.parent = parent;
    }

    /** Function to create panel with players in isolated threads */
    public void workWithCurrentThread(){
        JComboBox comboBox = new JComboBox();
        JCheckBox fps = new JCheckBox("fps");
        JButton snapShot = new JButton("shot");

        snapShot.setEnabled(false);
        fps.setEnabled(false);
        fps.setHorizontalAlignment(SwingConstants.CENTER);

        // set controls panel
        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridLayout(0,3));
        controlsPanel.add(snapShot);
        controlsPanel.add(comboBox);
        controlsPanel.add(fps);  // mock now. Change to fps and sound box later

        // set palyer
        EmbeddedMediaPlayerComponent playerComponent = new EmbeddedMediaPlayerComponent();
        playerComponent.getMediaPlayer().setRepeat(true);

        // add to container
        JPanel containerPanel = new JPanel();
        containerPanel.setLayout(new BorderLayout());
        containerPanel.add(playerComponent, BorderLayout.CENTER);
        containerPanel.add(controlsPanel, BorderLayout.SOUTH);

        parent.add(containerPanel);

        // Snap-listener makes shot and set it to write panel of the main frame.
        // Also creates the save-button to file system there, on the panel.
        snapShot.addActionListener(e -> getSnapshot(playerComponent.getMediaPlayer().getSnapshot()));

        // Parse json file on popup to set the list of available items in combo-box
        comboBox.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                ((JComboBox)e.getSource()).removeAllItems();
                videoFileChooser();
                for (int i = 0; i < boxItemsList.size(); i++)
                    ((JComboBox)e.getSource()).insertItemAt(boxItemsList.get(i).getName(), i);
            }
            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) { }
            @Override
            public void popupMenuCanceled(PopupMenuEvent e) { }
        });

        // Plays media by filepath from checked list item in the combo-box
        comboBox.addActionListener(e -> {
            if(((JComboBox)e.getSource()).getSelectedIndex() != -1){
                snapShot.setEnabled(true);
                fps.setEnabled(true);
                currentCamera = ((JComboBox)e.getSource()).getSelectedIndex();
                playerComponent.getMediaPlayer()
                        .playMedia(boxItemsList.get(((JComboBox)e.getSource()).getSelectedIndex()).getPath());
            }
        });

        // fps switcher
        fps.addItemListener(e -> {
            String fps1 = String.valueOf(playerComponent.getMediaPlayer().getFps());
            playerComponent.getMediaPlayer().setMarqueeText(fps1);
            if(e.getStateChange() == 1)
                playerComponent.getMediaPlayer().enableMarquee(true);
            else{
                playerComponent.getMediaPlayer().enableMarquee(false);
            }
        });

        // free resources
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                playerComponent.release();
                System.exit(0);
            }
        });
    }

    /**
     * parse json file to List of items that used in combo-box selections
     * @throws IOException
     * @throws URISyntaxException
     */
    public void videoFileChooser(){

        boxItemsList = new ArrayList<>();

        // итоговый конфиг оказывается в директории target/classes
        // так что для изменения списка доступых видео нужно менять его, а не тот что в /src
        JSONArray arr = Util.getJsonConfigJson().getJSONArray("videos");
        Item item;
        for (int i = 0; i < arr.length(); i++) {
            item = new Item();
            item.setName(arr.getJSONObject(i).getString("name"));
            item.setPath(arr.getJSONObject(i).getString("path_to_file"));
            boxItemsList.add(item);
        }
    }

    /**
     * функция отображения скриншота в правой части фрейма
     * coef - coefficient to scale picture for right panel display
     * right - panel with snapshot and save-button
     * @param img - snapshot tot save
     */
    public void getSnapshot(final BufferedImage img){

        int rightPanelWidth = frame.getContentPane().getComponent(1).getWidth(); //хм, странно. она по умолчанию перевернута. Player на компе ее так и воспроизводит. А vlc почему-то переворачивает как надо. Как он это узнает?
        float scaleCoefficient = (float) rightPanelWidth/(float) img.getWidth();
        frame.getContentPane().remove(1); // refreshing of right panel

        JButton saveInFileSystem = new JButton("save");
        JButton saveInFileSystemAsDefault = new JButton("saveAsDefault");
        JPanel saveButtons = new JPanel();
        JPanel right = new JPanel();

        saveButtons.setLayout(new GridLayout(0,2));
        saveButtons.add(saveInFileSystem);
        saveButtons.add(saveInFileSystemAsDefault);

        right.setLayout(new BorderLayout());
        right.add(saveButtons, BorderLayout.NORTH);
        right.add(new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0,rightPanelWidth, (int) (img.getHeight()*scaleCoefficient), null);
            }
        }, BorderLayout.CENTER);

        frame.add(right);
        frame.setVisible(true);
        saveInFileSystem.addActionListener(e -> saveScreenToFileSystem(img));
        saveInFileSystemAsDefault.addActionListener(e -> chooseFilePath(img, Util.getJsonConfigJson().getString("defaultPathForScreen")));
    }

    /**
     * create file chooser to switch folder for screen image
     * @param img - image for save
     */
    public void saveScreenToFileSystem(BufferedImage img)  {
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.addActionListener(e -> {
            if(e.getActionCommand() == JFileChooser.APPROVE_SELECTION){
                chooseFilePath(img,fc.getSelectedFile().getPath());
            }
        });
        fc.showSaveDialog(frame);
        // возвращает значение на закрытие окна save,close,error. Дальше можно обработать как-нибудь
        // можно поставить кнопку сэйв по дефолту (path - например прописать в кофиге)
        // int returnVal = fc.showSaveDialog(frame);
        // System.out.println(returnVal);
    }

    /**
     * helper to switch path for save buttons
     * @param img
     * @param path
     */
    public void chooseFilePath(BufferedImage img, String path){
        File resultScrinFile = new File(path
                + "\\"
                + boxItemsList.get(currentCamera).getName()
                + "_" + LocalDate.now()
                + "_" + LocalTime.now().getHour()
                + "-" + LocalTime.now().getMinute()
                + "-" + LocalTime.now().getSecond()
                + "" + ".png");

        try {
            ImageIO.write(img, "PNG", resultScrinFile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void run() {
        workWithCurrentThread();
    }
}
