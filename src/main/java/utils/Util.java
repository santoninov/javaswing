package utils;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;

public class Util {
    public static JSONObject getJsonConfigJson(){
        URL url = Util.class.getClassLoader().getResource("source.json");
        JSONTokener tokener = null;
        try {
            tokener = new JSONTokener(new FileReader(new File(url.toURI())));
        } catch (FileNotFoundException | URISyntaxException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject(tokener);
        return jsonObject;
    }
}
